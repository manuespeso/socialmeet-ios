
import Foundation

let somethingWrong = "something_wrong"
let addImage = "add_image"
let addUsername = "add_username"
let errorMessageImage = "error_message_image"

let mapAdvise = "map_advise_title"
let mapAdviseMessage = "map_advise_message"

let withoutMembersTitle = "without_members_title"
let withoutMembersMessage = "without_members_message"

let refreshAction = "refresh_action"

let signOutAction = "sign_out_accept"
let signOutCheck = "sign_out_check"
let cancelAction = "cancel_action"

let emailTextTitle = "email_text_title"
let emailTextMessage = "email_text_message"
let nameTextTitle = "name_text_title"
let nameTextMessage = "name_text_message"

let modificatePasswordTitle = "modificate_password_title"
let modificatePasswordMessage = "modificate_password_message"
let passwordElement = "password"

let passwordUpdatedTitle = "password_updated_title"
let passwordUpdatedMessage = "password_updated_message"


let placeTextTitle = "place_text_title"
let placeTextMessage = "place_text_message"
let streetTextTitle = "street_text_title"
let streetTextMessage = "street_text_message"
let dateTextTitle = "date_text_title"
let dateTextMessage = "date_text_message"

let cancel = "cancel"

extension String {
    func toLocalized() -> String {
        return NSLocalizedString(self,
                                 comment:"")
    }
}
