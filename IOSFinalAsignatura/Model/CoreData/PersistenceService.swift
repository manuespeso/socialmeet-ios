//
//  PersistanceService.swift
//  IOSFinalAsignatura
//
//  Created by Manu Espeso on 16/02/2020.
//  Copyright © 2020 Manuel Espeso Martin. All rights reserved.
//

import CoreData

class PersistenceService {
    
    private init (){}
    
    static var context: NSManagedObjectContext {
        return persistanceContainer.viewContext
    }
    
    static var persistanceContainer: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: "DataModel")
        container.loadPersistentStores(completionHandler: {(storeDescription, err) in
            
            if let err = err as NSError? {
                print("Unresolved error \(err), \(err.userInfo)")
            }
        })
        return container
    }()
    
    static func saveContext() -> Bool {
        
        let context = persistanceContainer.viewContext
        
        if context.hasChanges {
            do {
                
                try context.save()
                return true
            } catch {
                
                let nserror = error as NSError
                print("Unresolved error \(error), \(nserror.userInfo)")
                return false
            }
        } else {
            return false
        }
    }
}
